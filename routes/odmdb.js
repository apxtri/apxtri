const express = require("express");
const glob = require("glob");
const fs = require("fs-extra");
const path = require("path");
const conf = require(`../../../adminapi/objects/tribes/itm/adminapi.json`);
const Odmdb = require("../models/Odmdb.js");
// Middlewares
const checkHeaders = require("../middlewares/checkHeaders.js");
const isAuthenticated = require("../middlewares/isAuthenticated.js");
const router = express.Router();

/**
 * @api {get} /adminapi/odmdb/schemas/:tribe - objects Get
 * @apiGroup Odmdb
 * @apiName getIndex
 * @apiDescription Get objects available result is store in data.apx.conf for schema conf of adminapi schema (pagans,towns,... ) and data.apx.objectnames as array of schema name. Schema related to tribe are store in data.tribe.conf and data.tribe.objectnames where tribe come from header.xtribe
 * @apiParams {string} tribe to get list of schema related to tribe
 * @apiSuccess {object} contain data.indexname
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"objectslist", "data":{apx:{conf,objectnames:[]},tribe:{conf,objectnames:[]}}}
 */
router.get("/schemas/:tribe", checkHeaders, isAuthenticated, (req, res) => {
  const data = {
    tribename: req.params.tribe,
    apx: { conf: {}, objectnames: [] },
    tribe: { conf: {}, objectnames: [] },
  };
  let trb;
  glob.sync(`..{/,/../${req.params.tribe}/}schema/*.json`).forEach((f) => {
    const objectname = path.basename(f, ".json");
    console.log(f);
    trb = f.includes(req.params.tribe) ? "tribe" : "apx";
    if (objectname == "conf") {
      data[trb].conf = fs.readJSONSync(f);
    } else {
      data[trb].objectnames.push(objectname);
    }
  });
  res.status(200).json({ status: 200, ref: "Odmdb", msg: "objectslist", data });
});
/**
 * @api {get} /adminapi/odmdb/schema/:tribe/:objectname - Schema Get in the language header
 * @apiGroup Odmdb
 * @apiName getIndex
 * @apiDescription Get schema in the requested language if login have accessright. object $ref or $id are replace by the relevant schema, option $ref are replace by enum list of authorised value
 *
 * @apiParams {string} tribe (adminapi,smatchit,..) to looking for
 * @apiParams {string} objectname  requested must exist in adminapi or tribe
 * @apiSuccess {object} contain data.schema
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"schema", "data":{schema,objectname,lg}
 */
router.get(
  "/schema/:tribe/:objectname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    const objectPathname = `../../${req.params.tribe}/objects/${req.params.objectname}`;
    console.log(objectPathname);
    const retschema = Odmdb.Schema(objectPathname, true, req.header.xlang);
    res.status(retschema.status).json(retschema);
  }
);

/**
 * @api {get} /adminapi/odmdb/options/:tribe/objects/option/:optionname - Get option list in header language
 * @apiGroup Odmdb
 * @apiName getOption
 * @apiDescription Get schema in the requested language if login have accessright. object $ref or $id are replace by the relevant schema, option $ref are replace by enum list of authorised value
 *
 * @apiParams {string} tribe (adminapi,smatchit,..) to looking for
 * @apiParams {string} objectname  requested must exist in adminapi or tribe
 * @apiSuccess {object} contain data.schema
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"schema", "data":{schema,objectname,lg}
 */
router.get(
  "/options/:tribe/objects/option/:optionname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    const objectPathname = `../../${req.params.tribe}/objects/options/${req.params.optionname}_${req.header.xlang}.json`;
    if (fs.existsSync(objectPathname)) {
      res.status(200).json({
        status: 200,
        ref: "Odmdb",
        msg: "optionfind",
        data: fs.readJsonSync(objectPathname),
      });
    } else {
      res.status(404).json({
        status: 404,
        ref: "Odmdb",
        msg: "optionnotfound",
        data: { objectPathname },
      });
    }
  }
);

/** 
 * @api {get} adminapi/odmdb/idx/:tribe/:objectname/:indexname - index Get
 * @apiGroup Odmdb
 * @apiName getIndexbyindexname
 * @apiDescription Get index file for an object
 *
 * @apiParams {string} tribe if common adminapi or tribename 
 * @apiParams {string} objectname  If in conf.nationObjects then object is into nationchains/ else in tribes/xtribe/objectname/idx/indexname indexname
 * @apiParams {String} indexname  name of index file in /idx/indexnamme.json 
 *
 * 
 * @apiError {json} objectNotfound the file does not exist 
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
   {"status":404,"ref":"Odmdb","msg":"pathnamedoesnotexist","data":{indexpath}}
 *
 * @apiSuccess {object}  indexfile content
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"indexexist", "data":{indexname,content:{index file}}
 */
router.get(
  "/idx/:tribe/:objectname/:indexname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    console.log("passe");
    const indexpath = `../../${req.params.tribe}/objects/${req.params.objectname}/idx/${req.params.indexname}`;
    console.log(indexpath);
    if (fs.existsSync(indexpath)) {
      res.status(200).json({
        ref: "Odmdb",
        msg: "indexexist",
        data: {
          indexname: req.params.indexname,
          content: fs.readJsonSync(indexpath),
        },
      });
    } else {
      res.status(404).json({
        ref: "Odmdb",
        msg: "pathnamedoesnotexist",
        data: { indexpath },
      });
    }
  }
);

/**
 * @api {get} /adminapi/odmdb/rebuildidx/:tribe/:objectname  - index refresh all
 * @apiGroup Odmdb
 * @apiName refreshAllIndex
 * @apiDescription Rebuild all index for an object, this can be usefull in case crash or any data conflict. 
 *
 * @apiParam {string} objectname Mandatory 
 * @apiParam {string} tribe adminapi or tribe name (smatchit), where object is store
 * @apiSuccess {object}  indexfile content
 * @apiSuccessExample {json} successreindex
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"successreindex", "data":{"indexlist":[]}}
 *
 * @apiError {json} objectNotfound the file does not exist 
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
   {"status":404,"ref":"Odmdb","msg":"see nationchains/model/lg/Odmdb_xx.json","data":"object to render with msg"}
 *
 */
router.get(
  "/rebuildidx/:tribe/:objectname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    console.log("reindex");
    // check validity and accessright

    const objectPathname = `../../${req.params.tribe}/objects/${req.params.objectname}`;
    if (!fs.existsSync(objectPathname)) {
      res.status(404).json({
        status: 404,
        ref: "Odmdb",
        msg: "pathnamedoesnotexist",
        data: { indexpath: objectPathname },
      });
      return false;
    }
    if (
      conf.api.nationObjects.includes(req.params.objectname) &&
      !req.session.header.xprofils.includes("mayor")
    ) {
      res.status(403).json({
        status: 403,
        ref: "Odmdb",
        msg: "profilnotallow",
        data: { profils: "mayor" },
      });
      return false;
    }
    if (
      !conf.api.nationObjects.includes(req.params.objectname) &&
      !req.session.header.xprofils.includes("druid")
    ) {
      res.status(403).json({
        status: 403,
        ref: "Odmdb",
        msg: "profilnotallow",
        data: { profils: "druid" },
      });
      return false;
    }
    const reindex = Odmdb.idxfromitm(objectPathname, "I", {}, {}, [], {});
    res.status(reindex.status).json(reindex);
  }
);

/**
 * @api {post} /adminapi/odmdb/itm/:tribe/:objectname - item Create
 * @apiGroup Odmdb
 * @apiName postItm
 * @apiPermission none
 * @apiDescription Add an new item (data) into a collection of objectname items. Before Check data integrity with the relevant schema.
 * Then create a new primarykey. For multilanguage return see nationchains/model/lg/Odmdb_xx.json.
 * @apiParam {string} tribe adminapi or tribe name (smatchit), where object is store
 * @apiParam {string} objectname Place where to create new item, schema and version are available in /objectname/conf.json
 *
 * @apiBody {Object} data must pass Checkjson.js with schema
 *
 * @apiSuccess {json}  data idxprimary Value of idxprimary into objectname collection
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"cudsuccessfull", "data":{"itm":{}}}
 *
 * @apiError {json} schemanotfound The objectname schema is not found
 * @apiError {json} pathnamedoesnotexist The objectname does not exist for the tribe
 * @apiError {json} missingprimarykey Body data must have primarykey to be created
 * @apiError {json} unconsistencyapxidx some Body data get unique key that already exist
 * @apiError {json} checkjsonfalse The body data are not consistent with the schema
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
 * {"status":404,"ref":"Odmdb","msg":"see nationchains/model/lg/Odmdb_xx.json","data":"object to render with msg"}
 *
 */
router.post(
  "/itm/:tribe/:objectname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    // Create an item of an object with no specificities
    // if specificities then create a route / model that import odmdb
    const objectPathname = `../../${req.params.tribe}/objects/${req.params.objectname}`;
    const postitm = Odmdb.cud(objectPathname, "C", req.body, {
      xprofils: req.session.header.xprofils,
      xalias: req.session.header.xalias,
    });
    res.status(postitm.status).json(postitm);
  }
);

/**
 * @api {put} /adminapi/odmdb/itm/:tribe/:objectname - item Update
 * @apiGroup Odmdb
 * @apiName putItm
 * @apiPermission none
 * @apiDescription Update an item (data) into a collection of objectname items. Before Check data integrity with the relevant schema.
 * Then create a new primarykey. For multilanguage return see nationchains/model/lg/Odmdb_xx.json.
 * @apiParam {string} tribe adminapi or tribe name (smatchit), where object is store
 * @apiParam {string} objectname Place where to create new item, schema and version are available in /objectname/conf.json
 *
 * @apiBody {Object} data must pass Checkjson.js with schema
 *
 * @apiSuccess {json}  data idxprimary Value of idxprimary into objectname collection
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"cudsuccessfull", "data":{"itm":{}}}
 *
 * @apiError {json} schemanotfound The objectname schema is not found
 * @apiError {json} pathnamedoesnotexist The objectname does not exist for the tribe
 * @apiError {json} missingprimarykey Body data must have primarykey to be created
 * @apiError {json} unconsistencyapxidx some Body data get unique key that already exist
 * @apiError {json} checkjsonfalse The body data are not consistent with the schema
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
 * {"status":404,"ref":"Odmdb","msg":"see nationchains/model/lg/Odmdb_xx.json","data":"object to render with msg"}
 *
 */
router.put(
  "/itm/:tribe/:objectname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    // Create an item of an object with no specificities
    // if specificities then create a route / model that import odmdb
    const objectPathname = `../../${req.params.tribe}/objects/${req.params.objectname}`;
    const postitm = Odmdb.cud(objectPathname, "U", req.body, {
      xprofils: req.session.header.xprofils,
      xalias: req.session.header.xalias,
    });
    res.status(postitm.status).json(postitm);
  }
);

/**
 * @api {delete} /adminapi/odmdb/itm/:tribe/:objectname/:apxid/:primaryid - item Delete
 * @apiGroup Odmdb
 * @apiName deleteItm
 * @apiPermission none
 * @apiDescription Delete an item (data) into a collection of objectname items. This will reindex all idx for this object, but this won't check if this item.apxid is not already use somewhere else. If you use this endpoint be sure that no one will use it, if not then a get request will answer not found. For data integrity is better to use a scpecifc endpoint per tribe to manage a bunch of objects.
 * @apiParam {string} tribe adminapi or tribe name (smatchit), where object is store
 * @apiParam {string} objectname Place where to create new item, schema and version are available in /objectname/conf.json
 * @apiParam {string} apxid the key that is use as unique id for this object you find it in the schema.apxid
 * @apiBody {Object} data must pass Checkjson.js with schema
 *
 * @apiSuccess {json}  data idxprimary Value of idxprimary into objectname collection
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Odmdb", "msg":"cudsuccessfull", "data":{"itm":{}}}
 *
 * @apiError {json} schemanotfound The objectname schema is not found
 * @apiError {json} pathnamedoesnotexist The objectname does not exist for the tribe
 * @apiError {json} missingprimarykey Body data must have primarykey to be created
 * @apiError {json} unconsistencyapxidx some Body data get unique key that already exist
 * @apiError {json} checkjsonfalse The body data are not consistent with the schema
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
 * {"status":404,"ref":"Odmdb","msg":"see nationchains/model/lg/Odmdb_xx.json","data":"object to render with msg"}
 *
 */
router.delete(
  "/itm/:tribe/:objectname/:apxid/:primaryid",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    // Create an item of an object with no specificities
    // if specificities then create a route / model that import odmdb
    const objectPathname = `../../${req.params.tribe}/objects/${req.params.objectname}`;
    const itmtoremove={}
    itmtoremove[req.params.apxid]=req.params.primaryid;
    const delitm = Odmdb.cud(objectPathname, "D", itmtoremove, {
      xprofils: req.session.header.xprofils,
      xalias: req.session.header.xalias,
    });
    res.status(delitm.status).json(delitm);
  }
);

/**
 * @api {post} /adminapi/odmdb/searchitms/:tribe/:objectname - items Search
 * @apiGroup Odmdb
 * @apiName searchItms
 * @apiPermission none
 * @apiDescription Search dynamicaly in an object collection of items, results can be a list of apxid or an object {apxid1:{propertie:value,..}}. To get object you must specify in body the key fields. See usage example. TODO: This will evolve with deeper search technics
 * @apiParam {string} tribe name (smatchit), where object is store
 * @apiParam {string} objectname object name where looking for
 * @apiBody {array} [apxid] list of apxid prefilter when for perfomance you want filter the search investigation. If omit then it is looking for in any item object
 * @apiBody {array} [fields]  string "all" or list of properties of object you want to get (profils user must have accessright in Read for those fields, a value 'unauthorized' can be return in this case)
 * @apiBody {string} [condition] an array of condition {propertieA:[value1,value2],propertieB:[valu9],function:["name1",name2],...} select objectname id that match proprieteA = valu1 or value2 AND proprieteB=valu9 AND function name1(object) return true AND function name2(object) function are string code into schema  @TODO ...
 *
 * @apiSuccess {json} contain {status,ref,msg,data}
 * @apiSuccessExample {json} Return objects per id
 * HTTP/1.1 200 OK from body {apxid:[itmid],fields:[field1,field2]}
 * {"status":200, "ref":"Odmdb", "msg":"searchresult", "data":{"itms":{itmid:{field1,field2}]}}
 * @apiSuccessExample {json} Return arrays of ids
 * HTTP/1.1 200 OK from {question:[{"color":['blue','red']},{size:["<100"]},{function:name}]}
 * {"status":200, "ref":"Odmdb",  "msg":"searchresult", "data":[itm1,itm2]}
 * @apiError {json} objectdoesnotexist the obbject does not exist for the tribe
 * @apiError {json} unconsistentcondition condition format is not relevant
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
 * {"status":404,"ref":"Odmdb","msg":"templatestring Odmdb_xx.json","data":"object to render with msg"}
 *
 */
router.post(
  "/searchitms/:tribe/:objectname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    const objectPathname = `../../${req.params.tribe}/objects/${req.params.objectname}`;
    const ressearch = Odmdb.search(objectPathname, req.body, {
      xprofils: req.session.header.xprofils,
      xalias: req.session.header.xalias,
    });
    res.status(ressearch.status).json(ressearch);
  }
);

/**
 * @api {get} /adminapi/odmdb/itm/:tribe/:objectname/:primaryindex - item Get
 * @apiGroup Odmdb
 * @apiName getItemFromId
 * @apiDescription Get itm for a primaryid of an object
 * @apiParam {string} tribe adminapi or tribe name (smatchit), where object is store
 * @apiParam {String} objectname name Mandatory if in conf.nationObjects then file is into nationchains/ else in /tribes/xtribe/objectname
 * @apiParam {String} primaryindex the unique id where item is store
 *
 * @apiError {json} objectfiledoesnotexist the file item does not exist
 * @apiErrorExample {json}
 * HTTP/1.1 404 Not Found
 * {status:404,ref: "Odmdb",msg: "objectfiledoesnotexist",data: { objectpath }}
 * @apiSuccess {object}  indexfile content
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {status:200,ref:"Odmdb",msg:"itmfound", data:{itm:{} }} *
 *
 */
// indexname = objectname_key_value.json
router.get(
  "/itm/:tribe/:objectname/:primaryindex",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    const objectpath = `../../${req.params.tribe}/objects/${req.params.objectname}/itm/${req.params.primaryindex}.json`;

    if (fs.existsSync(objectpath)) {
      const readobj = Odmdb.r(
        `../../${req.params.tribe}/objects/${req.params.objectname}`,
        req.params.primaryindex,
        {
          xprofils: req.session.header.xprofils,
          xalias: req.session.header.xalias,
        }
      );
      res.status(readobj.status).json(readobj);
      /*res.status(200).json({
        status: 200,
        ref: "Odmdb",
        msg: "itmfound",
        data: fs.readJsonSync(objectpath),
      });*/
    } else {
      res.status(404).json({
        status: 404,
        ref: "Odmdb",
        msg: "objectfiledoesnotexist",
        data: { objectpath },
      });
    }
  }
);
/**
 * @api {get} https://anypaxtridns/adminapi/Checkjson.js  - schema Checkjson.js
 * @apiGroup Odmdb
 * @apiName checkjsonjs
 * @apiDescription Public js lib to import in a browser by :<br>
 * ```<script src="/adminapi/Checkjson.js"></script>```
 * to import in a node.js:<br>
 * ```const Checkjson = require(`Checkjson.js`);```
 *
 *  with functions:<br>
 *  <strong>Checkjson.schema.validation(schema)</strong> that return<br>
 *     - {status:200, ref:"Checkjson", msg:"validcheck"} => all rules are correct<br>
 *     - {status:406, multimsg:[{ref:"Checkjson",msg:"errorkey",data:{}}]}<br>
 *
 *  <strong>Checkjson.schema.data(schema{json},data{json},withschemacheck{boolean})</strong> that return<br>
 *     - {status:200, ref:"Checkjson", msg:"validcheck"} => all data keys respect schema rules <br>
 *     - {status:417, multimsg:[{ref:"Checkjson",msg:"errorkey",data:{}}]}<br>
 *
 * To identify issues, get the language errorkey list with a get
 *  
 * To check a property from an existing format
 * 
 * Checkjson.testformat=(str, format)=>{
 *  return null if format does not exist in Checkjson.schema.properties.format
 *  return true if str respect the format  (false in other case)
 */

/**
 * @api {get} https://wall-ants.ndda.fr/adminapi/schema/:objectname - schema Get public 
 * @apiGroup Odmdb
 * @apiName getPublicSchema
 * @apiDescription Get a Schema model from public apxtri (nations, pagans,persons,towns, tribes,wwws)
 * @apiSuccess {json} contain json file
 * @apiSuccessExample {json} Fichier direct
 * HTTP/1.1 200 Success-response:
  {
  "$id": "https://wall-ants.ndda.fr/schema/pagan",
  "$comment": "To create account bin apxtri",
  "title": "Pagans identity",
  "description": "A numeric id in the nationchains world",
  "type": "object",
  "properties": {
    "publickey": {
      "title": "Alias's publickey",
      "description": "Public key generate with openpgp.js",
      "type": "string",
      "format": "pgppublickey"
    },
    "alias": {
      "title": "Alias",
      "description": "text to remember easily a public key",
      "type": "string",
      "minLength": 4,
      "pattern": "^[a-z0-9]*$"
    },
    "dt_delete": {
      "title": "Date of death",
      "description": "Date of alias delete request, your will will be apply",
      "type": "string",
      "format": "date-time"
    },
    "will": {
      "title": "Will script after death",
      "description": "This will script will be apply on your data 30 days after your death",
      "type": "string"
    }
  },
  "required": ["publickey", "alias"],
  "apxid": "alias",
  "apxuniquekey": ["publickey"],
  "apxidx": [
    { "name": "lst_alias", "keyval": "alias" },
    { "name": "alias", "keyval": "alias" }
  ],
  "apxaccessrights": {
    "owner": { "R": [], "D": [] },
    "anonymous": { "C": [], "R": ["alias"] },
    "pagan": { "R": ["alias", "publickey"] }
  }
}

 * @apiError {json} contain json file
 * @apiErrorExample {string} nginx html not found message
 * HTTP/1.1 404 Not Found
 * <html>...</html>
**/

/**
 * @api {get} https://tribedns/:tribe/schema/:objectname.json  - schema Get from tribe
 * @apiGroup Odmdb
 * @apiName getPrivateSchema
 * @apiDescription Get a tribe schema or schema/lg/:objectname_xx.json this is served by nginx not express. tribedns is the domain name (ex:smatchit.io) 
 *
 * @apiParam {string} tribe where schema is stored ex;smatchit
 * @apiParam {String} objectname the json file name ex persons.json or for language /lg/persons_fr.json
 *
 * @apiError {object} ref objectname note found 
 * @apiErrorExample {json} Error-response:
 * HTTP/1.1 404 Not Found
 * <html>...</html>
 * @apiSuccess {json} data contains schema requested
 * @apiSuccessExample {json} Success-Response for : https://smatchit.io/smatchit/schema/persons.json
 * HTTP/1.1 200 Success-response:
 * {{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "/smatchit/schema/sirets",
  "title": "Siret is a legal french company",
  "description": "A recruiter can active a jobad if a maxnumber is not reach for this siret. Maxnumber is set depending of an offer or can be set manualy",
  "type": "object",
  "properties": {
    "siret": {
      "title": "siret",
      "description": "A unique string identifying a company ",
      "type": "string",
      "minLength": 14,
      "pattern": "^[0-9]*$"
    },
    "owner": {
      "title": "Owner of this siret",
      "description": "For accessright purpose this is set by the alias that pay the first time a subscription",
      "type": "string"
    },
    "dt_create": {
      "type": "string",
      "format": "date-time",
      "default": "dayjs.now()"
    },
    "dt_update": {
      "type": "string",
      "format": "date-time"
    },
    "subscription": {
      "title": "Offer subscribe",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "offer": { "type": "string", "enum": ["A", "B", "C", "FREE"] },
          "dt_payment": { "type": "string", "format": "date-time" }
        }
      }
    },
    "emailbilling":{
    	"type":"string",
    	"format":"email"
    },
    "namebilling":{
          "type":"string"
    },
    "maxactivejobad": {
      "title": "Number of active jobad at the same time",
      "description": "Alloaw by subscription payment for a date inside the last dt_payment and offer",
      "type": "integer"
    },
    "activejobad": {
      "title": "Current number of active jobadd",
      "type": "integer"
    },
    "frenchlocation": {
      "title": "Location",
      "description": "use franch gov refential to locaize",
      "$ref": "nationchains/schema/frenchlocation"
    },
    "denomination": {
      "title": "Company name",
      "type": "string"
    }
  },
  "required": ["siret"],
  "additionalProperties": true,
  "apxid": "siret",
  "apxuniquekey": ["siret"],
  "apxidx": [
    {
      "name": "lst_siret",
      "keyval": "siret"
    }
  ],
  "apxaccessrights": {
    "owner": {
      "D": [],
      "R": [
        "siret",
        "dt_create",
        "dt_update",
        "subscription",
        "maxactivejobadd",
        "activejobadd",
        "frenchlocation",
        "demomination"
      ],
      "U": ["frenchlocation", "demomination"]
    },
    "druid": {
      "C": [],
      "D": [],
      "R": [
        "siret",
        "dt_create",
        "dt_update",
        "subscription",
        "maxactivejobadd",
        "activejobadd",
        "frenchlocation",
        "demomination"
      ],
      "U": [
        "subscription",
        "maxactivejobadd",
        "activejobadd",
        "frenchlocation",
        "demomination"
      ]
    },
    "pagans": { "C": [] },
    "adminrecruiter": {
      "C": [],
      "R": [
        "siret",
        "dt_create",
        "dt_update",
        "subscription",
        "maxactivejobadd",
        "activejobadd",
        "frenchlocation",
        "demomination"
      ],
      "U": ["frenchlocation", "demomination"]
    },
    "recruiter": {
      "R": [
        "siret",
        "dt_create",
        "dt_update",
        "subscription",
        "maxactivejobadd",
        "activejobadd",
        "frenchlocation",
        "demomination"
      ]
    }
  }
}}
 */

/**
 * @api {put} https://wall-ants.ndda.fr/odmdb/schema/:tribe/:objectname - schema Put
 * @apiGroup Odmdb
 * @apiName putSchema
 * @apiDescription Replace a schema by another one need druid profil for a tribe
 *
 * @apiParam {String} objectname
 *
 * @apiBody {string} schemapath where to store schema .../schema
 * @apiBody {string} objectpath where to store object ...objectname/idx/conf.json
 * @apiBody {json} schema content
 * @apiBody {json} schemalang content in lg
 * @apiBody {string} lang define which schemalg is (2 letters)
 *
 * @apiError {object} ref objectmodel to get in the right language
 * @apiErrorExample {json} Error-response:
 * HTTP/1.1 404 Not Found
 * {"status":404,"ref":"Odmdb","msg":"schemanotfound","data":{"fullpath"}}
 *
 * @apiSuccess {object} data contains schema requested
 * HTTP/1.1 200 Success-response:
 * {"status":200,"data":{schema}}
 *
 *
 */
router.put(
  "schema/:tribe/:objectname",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    const fullpath = path.resolve(
      `${__dirname}/tribes/${req.session.header.xworkon}/schema/${req.params.pathobjectname}.json`
    );
    const set = Odmdb.setObject(
      path.resolve(`${__dirname}/tribes/${req.session.header.xworkon}`)
    );

    if (fs.existsSync(fullpath)) {
      res.status(200).json({ data: fs.readJsonSync(fullpath) });
    } else {
      res
        .status(404)
        .json({ msg: "schemanotfound", ref: "odmdb", data: { fullpath } });
    }
  }
);

/**
 * @api {get} https://wall-ants.ndda.fr/adminapi/objects/tplstring/:modelname_lg.json - translation notif Get public 
 * @apiGroup Odmdb
 * @apiName getPublicModelmessagekey 
 * @apiDescription Get a public json object for the ref: modelname in language lg, to get a template description with key msg
 * @apiParam {string} modelname Mandatory
 * @apiSuccess {json} contain json file
 * @apiSuccessExample {json} Fichier direct
 * HTTP/1.1 200 Success-response:
 *  {
  "alreadyexist": "Un object {{objectname}} avec la clé {{key}} existe déjà avec {{val}}",
  "doesnotexist": "L'object {{objectname}} avec la clé {{key}} ,'existe pas avec {{val}}",
  "getschema": "Schema {{{conf.name}}}",
  "schemanotfound": "Schema introuvable dans {{{schemaPath}}}",
  "pathnamedoesnotexist": "Le repertoire n'existe pas {{{indexpath}}}",
  "objectfiledoesnotexist": "Le fichier n'existe pas {{{objectpath}}}",
  "cudsuccessfull": "Mise à jour effectuée avec succés",
  "missingprimarykey": "Il manque une clé primaire apxid pour stocker et identifier les objects",
  "unconsistencyapxidx": "L'index {{name}} doit contenir en objkey au moins {{apxid}} car keyval n'est pas unique",
  "profilnotallow": "Vous n'avez pas le profil de {{profils}}, cette action n'est pas authorisée",
  "successreindex": "Objet reindexé à partir des items, vos index sont à jour",
  "indexexist":"L'indexe existe"
 * }
 * @apiError {json} contain json file
 * @apiErrorExample {string} nginx html not found message
 * HTTP/1.1 404 Not Found
 * <html>...</html>
**/

module.exports = router;

const express = require("express");
// Classes
const Notifications = require("../models/Notifications.js");

// Middlewares
const checkHeaders = require("../middlewares/checkHeaders");
const isAuthenticated = require("../middlewares/isAuthenticated");

const router = express.Router();

/**
 * wait Sagar feedback for language and label description
 * @ api {post} /api/notifications/backend - Notification Backend post
 * @apiName notifBackend
 * @apiDescription Send an api result {status,ref,msg,data} to get personnalize notification by header.xlang abd by data
 * @apiGroup Notification
 *
 * @apiBody {integer} status an http status
 * @apiBody {string}  ref an existing model name
 * @apiBody {string}  msg a key word existing in referentiual
 * */

router.post("/backend", (req, res) => {});

/**
 * @api {get} adminapi/notifications/messages/:alias/:tribeId -Get message list for alias in tribe
 * @apiName notiflist
 * @apiDescription Get list of notifications for an alias and a tribe
 * @apiGroup Notifications
 *
 * @apiParam {string} alias
 * @apiParam {string} tribeId
 * @apiSuccess {object}  notif content
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {status:200,ref:"Notification",msg:"Messagelist",data:{notif:[{tribeId,msg:{from(email or uuid or alias):[{dt,msg}]}}]}
 * bouture
 **/
router.get("/messages/:alias/:tribeId", (req, res) => {
  const getnot = Notification.get(req.params.alias, req.params.tribeId);
  res.status(getalias.status).send(getalias);
});

/**
 * @api {POST} adminapi/notifications/sendmail/:tribe/:template -Send personnalize emails
 * @apiName Sendmail
 * @apiDescription Send personnalize email with data from template store in ../../{tribe}/template/{template}.json and smtp in conf global or in /itm/{tribe}.json that must have valid parameter emailcontact must be authorized by the smtp
   *  "emailcontact": "noreply@smatchit.io",
   *  "smtp": {
   *    "host": "smtp-relay.brevo.com",
   *    "port": 587,
   *    "secure": false,
   *    "auth": {
   *     "user": "xx",
   *    "pass": "yy"
   *    }
   *   }

 * @apiGroup Notifications
 *
 * @apiParam {string} template
 * @apiParam {string} tribe
 * @apiBody {array} emails to send (array of valid email)
 * @apiBody {object} data to personnalize template
 *
 * @apiSuccess {object}  notif content
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * {status:200,ref:"Notification",msg:"Messagelist",data:{notif:[{tribeId,msg:{from(email or uuid or alias):[{dt,msg}]}}]}
 * bouture
 */
router.post(
  "/sendmail/:tribe/:template",
  checkHeaders,
  isAuthenticated,
  async (req, res) => {
    const data = req.body.data;
    data.emailsto = req.body.emails;
    const pathtpl = `../../${req.params.tribe}/template/${req.params.template}_${req.session.header.xlang}.js`;
    const sendemail = await Notifications.manageemail(
      data,
      pathtpl,
      req.params.tribe
    );
    res.status(sendemail.status).send(sendemail);
  }
);

/**
 * @api {POST} adminapi/notifications/registeranonymous/:tribe/:mlist/:typekey/:data -Register email||phone to mlist
 * @apiName register
 * @apiGroup Notifications
 * @apiDescription Register an email or phone into a mailinglist mlist
 * @apiBody {string} tribe an existing tribe
 * @apiBody {string} mlist a mailing list name
 * @apiBody {string} key email or phone keyword
 * @apiBod {string} srckey must exist in tribes/schema/lg/enumtrk_xx.json
 * @apiParams {string} data the email or phone value
 * @apiSuccess {object}  update mailinglist/{mlist}.json successfull
 * @apiSuccessExample {json} successfullmessage
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Notifications", "msg":"registersuccess", "data":{data, typekey, tribe, mlist, srckey, uuid}}}
 *
 */
router.post("/registeranonymous", checkHeaders, (req, res) => {
  //console.log("list registration ",req.body)
  if (!req.body.typekey || !["email", "telephone"].includes(req.body.typekey)) {
    return res.status(406).json({
      status: 406,
      ref: "Notifications",
      msg: "typekeyunknown",
      data: { typekey: req.body.typekey },
    });
  }
  const key = req.body.contactpoint
    ? req.body.contactpoint
    : req.body[req.body.typekey];
  result = Notifications.registertolist(
    key,
    req.body.typekey,
    req.body.tribe,
    req.body.mlist,
    req.body.srckey,
    req.session.header.xuuid
  );

  res.status(result.status).json(result);
});
/**
 * @api {GET} adminapi/notifications/unregister/:tribe/:mlist/:typekey/:data/:validation -Unregister email or phone from a mlist or all mlist
 * @apiName unregister
 * @apiGroup Notifications
 * @apiDescription Register an email into a mailinglist mlist
 * @apiParams {string} tribe an existing tribe
 * @apiParams {string} mlist a mailing list name
 * @apiParams {string} key email or phone
 * @apiParams {string} srckey must exist in tribes/schema/lg/enumtrk_xx.json
 * @apiParams {string} data the email or phone
 * @apiParams {string} validation  a key store in /tmp waiting to be approved, if "request" then send an email for confirmation, else if exist then remove email from list.
 * @apiSuccess {object}  update mailinglist/{mlist}.json successfull
 * @apiSuccessExample {json} successfullmessage
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Notifications", "msg":"registersuccess", "data":{data, typekey, tribe, mlist, srckey, uuid}}}
 *
 */

router.get(
  "/unregister/:tribe/:mlist/:typekey/:srckey/:data/:validation",
  checkHeaders,
  (req, res) => {
    Notifications.registertolist = (typekey, tribe, mlist, srckey, uuid);
    result = Notifications.registertolist(
      req.params.typekey,
      req.params.tribe,
      req.params.mlist,
      req.params.srckey,
      req.session.header.xuuid
    );
    res.status(result.status).json(result);
  }
);

/**
 * @api {GET} adminapi/notifications/stat/maillinglst/:tribe --Statistic maillinglst
 * @apiName statmaillinglst
 * @apiGroup Notifications
 * @apiDescription Mailling list statistique
 * @apiParams {string} tribe an existing tribe
 *
 * @apiSuccess {object}  with data results
 * @apiSuccessExample {json} successfullmessage
 * HTTP/1.1 200 OK
 * {
 *  "status": 200,
 *   "ref": "Notifications",
 *   "msg": "statistics",
 *   "data": {
 *       "teasingwebpage": {
 *           "email_stayinformboth": 1,
 *           "email_stayinformseeker": 6
 *       },
 *       "gorillamkt": {
 *           "email_trouvemonjobrecruiter": 7,
 *           "email_trouvemonjobseeker": 142,
 *           "telephonefr_trouvemonjobrecruiter": 4,
 *           "telephonefr_trouvemonjobseeker": 103
 *       },
 *       "forumantony": {
 *           "email_trouvemonjobseeker": 18,
 *           "telephonefr_trouvemonjobseeker": 151
 *      }
 *   }
 *}
 *
 */
router.get("/stat/maillinglst/:tribe", checkHeaders, async (req, res) => {
  console.log("passe là");
  const resstat = Notifications.statmaillist(req.params.tribe);
  res.status(resstat.status).json(resstat);
});

/**
 * @api {POST} adminapi/notifications/contactanonymous/:tribe -Contact anonymous
 * @apiName contactanonymous
 * @apiGroup Notifications
 * @apiDescription Register a contact in tribe and send a mail to admin of tribe
 * @apiParams {string} tribe an existing tribe
 * @apiBody {string} srckey: where it come from and eventualy email template name to use to send email ,
 * @apiBody {string} email to recontact
 * @apiBody {string} others any other usefull key:value
 *
 * @apiSuccess {object}  create/update tribe/contacts/{}.json successfull
 * @apiSuccessExample {json} successfullmessage
 * HTTP/1.1 200 OK
 * {"status":200, "ref":"Contact", "msg":"success", "data":{"indexlist":[]}}
 *
 */
router.post("/contactanonymous", checkHeaders, async (req, res) => {
  const done = Actions[req.body.order]
    ? await Actions[req.body.order](req.body, req.session.header)
    : { status: 406, ref: "Actions", msg: "bodyerror", data: req.body };
  //console.log('routes contactanonymous ', req.body);
  res.status(done.status).json(done);
});
/**
 * Same as /copntactanonymous but for authenticated user => data are updated in persons/itm/alias.json
 */
router.post(
  "/contact/:tribe/:alias",
  checkHeaders,
  isAuthenticated,
  (req, res) => {
    const done = Actions[req.body.order]
      ? Actions[req.body.order](req.body, req.session.header)
      : { status: 406, ref: "Actions", msg: "bodyerror", data: req.body };
    console.log(req.body);
    res.status(done.status).json(done);
  }
);

router.get("/contact", checkHeaders, isAuthenticated, (req, res) => {
  res.status(200).json({ data: {} });
});

module.exports = router;

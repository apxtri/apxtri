const { argv } = require("process");
const fs = require("fs-extra");
const mustache = require("mustache");
const glob = require("glob");
const path = require("path");

const Setup = {};

Setup.dnstown=["apxtri.crabedance.com","apxtri.mooo.com"]

Setup.nginxconf = `
user  {{user}};
##########################################
## This file is auto generate by apXtri ##
##########################################
worker_processes  auto;
error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;
#include /etc/nginx/modules-enabled/*.conf;
events { worker_connections  1024;}
http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    log_format  main  '$time_iso8601###$status###$request';
    log_format  tracker escape=json
                  '{"time":"$time_iso8601","alias":"$arg_alias","uuid":"$arg_uuid",'
                  '"lg":"$arg_lg","consentcookie":"$arg_consentcookie","version":"$arg_version",'
                  '"srckey":"$arg_srckey","request_filename":"$request_filename",'
                  '"remoteaddr":"$remote_addr","httpxforwardedfor":"$http_x_forwarded_for",'
                  '"httpreferer":"$http_referer","httpuseragent":"$http_user_agent","request":"$request"}';
    log_format  mainold  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;
    sendfile        on;
    keepalive_timeout  65;
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 4 32k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/x-font-ttf application/javascript font/eot font/opentype image/svg+xml image/x-icon;
    ##
    # Virtual Host Configs
    ##
    #include /etc/nginx/conf.d/*.conf;
    include {{apxtownsfolder}}/apxtowns/*/*/nginx/*.conf;
}
`;
Setup.adminapinginxconf = `
server {
    server_name {{#dns}} {{.}} {{/dns}};
    # usefull to debug nginx conf 3 next line:
    access_log {{/apxtowns/{{town}}-{{nation}}/{{tribe}}/logs/nginx/{{tribe}}_{{webspace}}.access.log main;
    #error_log {{apxtownsfolder}}//{{tribe}}/logs/nginx/{{tribe}}_{{webspace}}.error.log debug;
    #add_header xdebug "testmsg debug:  $uri - $request - liste args: $args -  url:$arg_url - alias:$arg_alias  " always;
    
    set $trackme 0; 
    if ( $uri ~ ^/trk/ ){
        set $trackme 1;
    }
    #access_log {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/logs/nginx/{{tribe}}_{{webspace}}.trk.log tracker if=$trackme ;
    access_log {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/logs/nginx/{{tribe}}_{{webspace}}.trk.log tracker if=$trackme ;
    location ~* /trk/ { 
        if ( $uri ~ ^/trk/redirect ){
            return 301 $arg_url; 
        }
        rewrite ^/trk/(.*)$ /$1;
    }   
    location /adminapi/Checkjson.js {
        alias {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/adminapi/apxtri/models/Checkjson.js;
    }
    location ~* /adminapi/objects/tplstrings/ {
        rewrite /adminapi/objects/tplstrings/(.*$) /$1 break;
        root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/adminapi/objects/tplstrings/;
    }
    location ~* /adminapi/schema/ {
        rewrite /adminapi/schema/(.*$) /$1 break;
        root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/adminapi/schema/;
    }    
    location ~* /{{tribe}}/objects/tplstrings/ {
        rewrite /{{tribe}}/objects/tplstrings/(.*$) /$1 break;
        root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/objects/tplstrings/;
    }
    location ~* /{{tribe}}/schema/ {
            rewrite /{{tribe}}/schema/(.*$) /$1 break;
            root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/schema/;
    }    
    location /cdn/ {
        expires 1y;
        add_header Cache-Control "public";
        rewrite /cdn/(.*$) /$1 break;
        root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/objects/wwws/cdn/;
    }
    location /api/ {
        rewrite /api/(.*$) /$1 break;
        proxy_pass http://localhost:3020;
        proxy_redirect off;
        include proxy_params;
    }
    //ASUP 
    location /apxwebapp/ {
        rewrite /apxwebapp/(.*$) /$1 break;
        root {{apxtownsfolder}}/apxtowns/apxwebapp;
        index index.html index_fr.html index_en.html;
    }
    location /apidoc/ {
        root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/objects/wwws/;
        index index.html;
    }
    location / {
        root {{apxtownsfolder}}/apxtowns/{{town}}-{{nation}}/{{tribe}}/objects/wwws/{{webspace}};
        index index.html index_fr.html index_en.html;
    }
    error_page 404 /404.html;
    # redirect server error pages to the static page /50x.html
    #
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/local/nginx/html;
    }
}
`;

Setup.addtribe = (tribeId, town, nation) => {
  const idxtribf = `../../idx/tribeId_all.json`;
  const idxtrib = fs.existsSync(idxtribf) ? fs.readJSONSync(idxtribf) : {};
  if (idxtrib[tribeId]) {
    return {
      status: 406,
      ref: "Tribes",
      msg: "alreadyexist",
      data: { tribeId },
    };
  }
  const tribdata = {
    nationId: nation,
    townId: town,
    tribeId: tribe
  };
  idxtrib[tribeId] = tribdata;
  fs.outputJSON(idxtribf, idxtrib, { spaces: 2 });
  fs.outputJSON("../../adminapi/objects/tribes/itm/${tribeId}.json", tribdata, { space: 2 });
  [
    `../../${tribeId}/nginx`,
    `../../${tribeId}/logs/nginx`,
    `../../${tribeId}/objects`,
    `../../${tribeId}/schema`,
  ].forEach((d) => fs.ensureDirSync(d));
  return {
    status: 200,
    ref: "Tribes",
    msg: "tribeaddsuccess",
    data: { tribe: tribeId },
  };
};
Setup.addwww = (tribeId,webspace,confdata) => {
  fs.outputFile(
    "/etc/nginx/nginx.conf",
    mustache.render(Setup.nginxconf, confdata),
    "utf-8"
  );
  fs.outputFile(
    `${confdata.apxtowns}/${tribeId}/nginx/${tribeId}_${webspace}.conf`,
    mustache.render(Setup.adminapinginxconf, confdata),
    "utf-8"
  );
  const res={status:200,ref:"setup",msg:"wwwssuccess",data:confdata}
  const { exec } = require("child_process");
  exec("sudo systemctl restart nginx", (error, stdout, stderr) => {
    if (error) {
      res.status = 500;
      res.msg = "nginxError";
      res.data = { msg: `${error}<br>${stdout}<br>${stderr}` };
      return res
    } else {
        // for production exec("sudo certbot --nginx -d dns[0] -d dns[1] ...") or do it from adminapx
        console.log(
          `\x1b[42m###########################################################################################\x1b[0m\n\x1b[42mWellcome into apxtri, you can now 'yarn dev' for dev or 'yarn startpm2' for prod or       \n'yarn unittest' for testing purpose. Access to your town here \x1b[0m\x1b[32mhttp://adminapx\x1b[0m \x1b[42m             \nto finish your town setup. Don't forget to set your localhost /etc/hosts by adding 127.0.0.1 adminapx or {LAN IP} adminapx .  Check README's project to learn more.                          \x1b[0m\n\x1b[42m###########################################################################################\x1b[0m`
        );
        return res
    }
  });
};

Setup.init = (param) => {
  const nam = path.resolve("../..").split("/").slice(-1)[0].split("-");
  const confdata = {
    town: nam[0],
    nation: nam[1],
    apxtownsfolder: path.resolve("../../../.."),
  };
  let setupdone = true;
  // check nginx exist
  if (!fs.existsSync("/etc/nginx/nginx.conf")) {
    setupdone = false;
    console.log(
      "\x1b[31m Check documentation, nginx have to be installed on this server first, no /etc/nginx/nginx.conf available, install then rerun yarn command."
    );
  }
  if (!(param.dns && param.user)) {
    setupdone = false;
    console.log(
      "This is a first install you must run next command with a domain name (use town-nation for dev and the linux user that run apXtri) check ping dns answer and su user exist (check README) \n dns=domainname user=linuxuser yarn dev"
    );
  }
  if (!setupdone) return false;
  confdata.dns = [param.dns];
  confdata.user = param.user;
  const addtribe = Setup.addtribe("adminapi", confdata.town, confdata.nation);
  if (addtribe.status != 200) {
    console.log("erreur de creation");
    return false;
  }
  // set nginx
  const addwww = Setup.addwww("adminapi","admibnapx",confdata);
};

const param = {};
argv.slice(2).forEach((arg) => {
  const kv = arg.split("=");
  if (kv.length == 2) {
    param[kv[0]] = kv[1];
  }
});
if (Setup.init(param))
  console.log(
    "Your town was set successfully. Run yarn dev or yarn startapx for production"
  );

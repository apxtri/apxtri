/*
Unit testing
*/
const assert = require("assert");
const Checkjson = require("../Checkjson.js");

const ut = { name: "Checkjson" };

const schema = {
  $schema: "http://json-schema.org/schema#",
  title: "Dummy schema to test Checkjson.js",
  description: "Checkjson is use on server as well as into a browser",
  $comment: "We change schema type on the fly to simplify the test",
  type: "object",
  properties: {
    totest: {},
  },
};

const testproperties = [
  {
    name: "test0",
    data: { totest: true },
    properties: { totest: { type: "boolean" } },
    status: 200,
  },
  {
    name: "test1",
    data: { totest: "blabla" },
    properties: { totest: { type: "string" } },
    status: 200,
  },
  {
    name: "test2",
    data: { totest: 123 },
    properties: { totest: { type: "string" } },
    status: 417, 
  },
  {
    name: "test3",
    data: { totest: 123.13 },
    properties: { totest: { type: "integer" } },
    status: 200, // Should be 417
  },
  {
    name: "test4",
    data: { totest: "short" },
    properties: { totest: { type: "string", minLength: 10 } },
    status: 417,
  },
];

ut.testproperties = (options) => {
  let msg = "";
  testproperties.forEach((t) => {
    schema.properties = t.properties;
    const res = Checkjson.schema.data(schema, t.data);
    if (res.status != t.status) {
      msg = msg == "" ? "Unconsistent testproperties() name list: " : `${msg},`;
      if (options.verbose) {
        console.log(t);
        console.log(res);
      }
      msg += ` ${t.name} expected status ${t.status}, got ${res.status}`;
    }
  });
  return assert.deepEqual(msg, "", msg);
};

ut.run = (options) => {
  console.log("Test Checkjson properties");
  ut.testproperties(options);
};

module.exports = ut;

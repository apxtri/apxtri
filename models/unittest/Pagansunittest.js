const assert = require('assert');
const openpgp = require('openpgp');
const Pagans = require('../Pagans');
const dayjs = require('dayjs');

const ut = { name: 'Pagans' };

// Data
ut.test = {
  tribe: "smatchit",
  pagans: [
    {
      alias: "unittestadminsmatchit",
      passphrase: "adminsmatchitPass",
      persons: { firstname: "toto", lastname: "titi", profils: ["anonymous"] },
      testprofil: "adminrecruiter",
    },
    {
      alias: "unittestseeker",
      passphrase: "",
      persons: { firstname: "toto", lastname: "titi", profils: ["anonymous"] },
      testprofil: "seeker",
    },
    {
      alias: "unittestrecruiter",
      passphrase: "recruiterPass",
      persons: { firstname: "toto", lastname: "titi", profils: ["anonymous"] },
      testprofil: "recruiter",
    },
    {
      alias: "unittestadminrecruiter",
      passphrase: "adminrecruiterPass",
      persons: { firstname: "toto", lastname: "titi", profils: ["anonymous"] },
      testprofil: "adminrecruiter",
    },
  ],
};

// Apx
const apx = {};
apx.data = {};
apx.data.headers = {};

apx.generateKey = async (alias, passphrase) => {
  try {
    console.log(`\nGenerating keys for alias: ${alias}`);
    const pgpParams = {
      type: "ecc",
      curve: "curve25519",
      userIDs: [{ name: alias }],
      passphrase: passphrase,
      format: "armored",
    };

    const key = await openpgp.generateKey(pgpParams);
    console.log(`Keys generated successfully for alias: ${alias}`);

    return {
      alias,
      passphrase,
      privatekey: key.privateKey,
      publickey: key.publicKey,
    };
  } catch (error) {
    console.error(`Error generating keys for alias: ${alias}`, error);
    return {};
  }
};

apx.createIdentity = async (alias, passphrase) => {
  try {
    const { privatekey, publickey } = await apx.generateKey(alias, passphrase);
    if (!privatekey || !publickey) {
      throw new Error(`Failed to generate keys for ${alias}`);
    }
    console.log(`Identity created successfully for alias: ${alias}`);
    return { alias, privatekey, publickey };
  } catch (error) {
    console.error(`Error creating identity for alias: ${alias}`, error);
    throw error;  
  }
};

apx.clearmsgSignature = async (privateKeyArmored, passphrase, message) => {
  try {
    const privateKey = await openpgp.readPrivateKey({ armoredKey: privateKeyArmored });
    let decryptedPrivateKey = privateKey;

    if (!privateKey.isDecrypted()) {
      decryptedPrivateKey = await openpgp.decryptKey({
        privateKey,
        passphrase
      });
    }

    const signedMessage = await openpgp.sign({
      message: await openpgp.createMessage({ text: message }),
      signingKeys: decryptedPrivateKey
    });

    return signedMessage;
  } catch (error) {
    console.error('Error signing message:', error);
    return null;
  }
};

apx.authenticate = async (alias, passphrase, privatekey) => {
  try {
    apx.data.headers.xalias = alias;
    apx.data.headers.xdays = dayjs().valueOf();
    const msg = `${alias}_${apx.data.headers.xdays}`;

    console.log("pvk", privatekey);
    apx.data.headers.xhash = await apx.clearmsgSignature(privatekey, passphrase, msg);
    if (!apx.data.headers.xhash) {
      throw new Error('Failed to generate xhash for authentication');
    }

    console.log(`Authentication successful for alias: ${alias}`);
    return apx.data.headers;
  } catch (error) {
    console.error(`Error authenticating alias: ${alias}`, error);
    throw error;
  }
};


ut.createIdentity = async (t) => {
  try {
    // Test if alias already exists
    const getalias = await Pagans.getalias(t.alias);
    if (getalias.status !== 404) {
      console.log(`Alias ${t.alias} already exists. Deleting...`);
      await Pagans.deletealias(t.alias);
      console.log(`Deleted ${t.alias}.`);
    }

    // Generate keys
    const keys = await apx.createIdentity(t.alias, t.passphrase);
    if (!keys.privatekey || !keys.publickey) {
      throw new Error(`Failed to generate keys for ${t.alias}`);
    }

    return keys;
  } catch (error) {
    throw new Error(`Error creating identity for ${t.alias}: ${error.message}`);
  }
};

// Authentication process
ut.authenticate = async (t, privatekey, passphrase) => {
  try {
    const headers = await apx.authenticate(t.alias, passphrase, privatekey);
    return headers;
  } catch (error) {
    throw new Error(`Error authenticating ${t.alias}: ${error.message}`);
  }
};

ut.run = async () => {
  console.log('Test Pagans Registration and Authentication');

  for (const t of ut.test.pagans) {
    try {
      console.log(`Creating identity for ${t.alias}`);
      const identity = await ut.createIdentity(t);

      console.log(`Authenticating ${t.alias}`);
      const headers = await ut.authenticate(t, identity.privatekey, t.passphrase);
      console.log(`Headers for ${t.alias}:`, headers);

      console.log(`All operations for ${t.alias} completed successfully.`);
    } catch (error) {
      console.error(`Error processing ${t.alias}: ${error.message}`);
    }
  }

  console.log('All test cases ran successfully');
};

module.exports = ut;


if (require.main === module) {
  ut.run().catch(err => {
    console.error('Test case failed:', err);
  });
}

const fs = require("fs-extra");
const path = require("path");
const dnsSync = require("dns-sync");
const mustache = require("mustache");
const readlineSync = require("readline-sync");
const Odmdb = require("./Odmdb.js");
const conf = require(`../../../adminapi/objects/tribes/itm/adminapi.json`);
const Wwws = {};

Wwws.initlocalwco=(tribwco,profils,lg)=>{
  const wco={}
  Object.keys(tribwco).forEach(t=>{
    if (!fs.existsSync(`../../../${tribwco}`)){}

  })
}


Wwws.initlocaldata = (tribe, appname, pagename, version, profils, lg) => {
  const fileparam = `../../${tribe}/objects/wwws/itm/${appname}.json`;
  console.log(fileparam);
  if (!fs.existsSync(fileparam)) {
    return { status: 404, ref: "Wwws", msg: "appdoesnotexist", data: {} };
  }
  const locals = fs.readJSONSync(fileparam);
  if (!locals.pages[pagename]) {
    return {
      status: 200,
      ref: "Wwws",
      msg: "pagedoesnotexist",
      data: { pagename }
    };
  }
  if (locals.pages[pagename].version == version) {
    return { status: 200, ref: "Wwws", msg: "nonewdatamodel", data: {} };
  }
  let authorize = false;
  profils.forEach((p) => {
    authorize = authorize || locals.pages[pagename].profils.includes(p);
  });
  if (!authorize) {
    return {
      status: 200,
      ref: "Wwws",
      msg: "forbidenaccess",
      data: { pagename, profils },
    };
  }
  //check version
  const initname = `../../tmp/initlocaldata/${tribe}_${appname}_${pagename}_${lg}_${locals.pages[pagename].version}.json`;
  if (fs.existsSync(initname) && 1 != 1) {
    const init = fs.readJsonSync(initname);
    if (init.app.version == locals.app.version) {
      return { status: 200, ref: "Wwws", msg: "datamodelnoupdate", data: init };
    }
  }
  const localstorage = {
    version: locals.pages[pagename].version,
    headers: locals.apxtri.headers,
    confpage:locals.pages[pagename].confpage,
    req: {},
    itm:{},
    itms: {},
    options: {},
    tpl: {},
    tpldata:{},
    ref: {},
    schema: {},
  };
  localstorage.headers.xlang = lg;
  // A faire plus tard charger tous les referentiele et les data pour une page adminpage
 /* if (pagename=="pageadmin"){
    // load any referentialdata
    glob.Sync(`../../${tribe}/objects/*.json`).forEach(f=>{
      if (!localstorage.schema.includes(`${tribe}/objects/${path.basename(f,".json")}`)){
        localstorage.schema.push(`${tribe}/objects/${path.basename(f,".json")}`)
      }
    })

  }
  */
  const loc = locals.pages[pagename];
  if (loc.itms){
    Object.keys(loc.itms).forEach((r) => {
      const src = `../../${loc.itms[r]}.json`;
      console.log("hhhhhhhhhhhhhh",path.resolve(src))
      if (fs.existsSync(src)) {
        localstorage.itms[r] = fs.readJSONSync(src);
      } else {
        localstorage.itms[r] = `Check your ${fileparam}.json for itms in ${pagename} and ${r}`;
      }
    });
  }
  if (loc.ref) {
    Object.keys(loc.ref).forEach((r) => {
      const src = `../../${loc.ref[r]}_${lg}.json`;
      if (fs.existsSync(src)) {
        localstorage.ref[r] = fs.readJSONSync(src);
      } else {
        localstorage.ref[
          r
        ] = `Check your ${fileparam}.json for ref in ${pagename} and ${r}`;
      }
    });
  }
  if (loc.options) {
    Object.keys(loc.options).forEach((r) => {
      const src = `../../${loc.options[r]}_${lg}.json`;
      if (fs.existsSync(src)) {
        localstorage.options[r] = fs.readJSONSync(src);
      } else {
        localstorage.options[
          r
        ] = `Check your ${fileparam} for options in ${pagename} profil and ${r}`;
      }
    });
  }
  if (!loc.tpl) loc.tpl={};
  if (!loc.tpldata) loc.tpldata={};
  if (loc.components){
    loc.components.forEach(c=>{
      const componame=path.basename(c)
      loc.tpl[componame]=`${c}/${componame}.mustache`
      loc.tpldata[componame]=`${c}/${componame}`
    })        
  }
  if (loc.tpl) {
    Object.keys(loc.tpl).forEach((r) => {
      let src = `../../${loc.tpl[r]}`;
      console.log(path.resolve(src))
      if (!fs.existsSync(src)) {
        src += `_${lg}.mustache`;
      }
      if (fs.existsSync(src)) {
        localstorage.tpl[r] = fs.readFileSync(src,'utf-8');
      } else {
        localstorage.tpl[
          r
        ] = `Check your ${fileparam} for template in ${pagename} profil and ${r}`;
      }
    });
  }
  if (loc.tpldata) {
    Object.keys(loc.tpldata).forEach((r) => {
      let src = `../../${loc.tpldata[r]}`;
      console.log(path.resolve(src))
      if (!fs.existsSync(src)) {
        src += `_${lg}.json`;
      }
      if (fs.existsSync(src)) {
        localstorage.tpldata[r] = fs.readJSONSync(src);
      } else {
        localstorage.tpldata[
          r
        ] = `Check your ${fileparam} for template in ${pagename} profil and ${r} in tpldata`;
      }
    });
  }
  if (loc.schema) {
    loc.schema.forEach((objpath) => {
      const name = path.basename(objpath);
      const schema = Odmdb.Schema(`../../${objpath}`, false, lg);
      if (schema.status == 200) {
        localstorage.schema[name] = schema.data.schema;
      } else {
        console.log(schema);
        let msg=`Check your ${fileparam} for schema in ${pagename}, for ${schema.status} ${schema.ref} ${schema.msg} :`
        if (schema.status==406){
          if (schema.msg) msg+=schema.msg
          if (schema.multimsg) {
            schema.multimsg.forEach(err=>{
              msg+=`${err.msg} ${JSON.stringify(err.data)})`
            })
          }
        }
        localstorage.schema[
          name
        ] = msg;
      }
    });
  }
  return {
    status: 200,
    ref: "Wwws",
    msg: "datamodelupdate",
    data: localstorage 
  };
};

/*Wwws.apxtriinstall = (paramconf) => {
  if (fs.existsSync(`${conf.dirtown}/conf.json`)) {
    console.log("You already have a conf on this town");
    process.exit();
  }
  //first install
  const nginxconf = fs.readFileSync(
    `${conf.dirapi}/adminapi/www/adminapx/conf/nginx.conf.mustache`,
    "utf8"
  );
  const proxyparams = fs.readFileSync(
    "../nationchains/www/adminapx/static/tpl/nginxproxy_params.mustache",
    "utf8"
  );
  // saved and change nginx conf
  if (!fs.existsSync("/etc/nginx/nginxconf.saved")) {
    fs.moveSync("/etc/nginx/nginx.conf", "/etc/nginx/nginxconf.saved");
    console.log(
      "your previous /etc/nginx/nginx.conf was backup in  /etc/nginx/nginxconf.saved"
    );
  }
  fs.outputFileSync(
    "/etc/nginx/nginx.conf",
    mustache.render(nginxconf, paramconf),
    "utf8"
  );
  fs.outputFileSync(
    "/etc/nginx/proxy_params",
    mustache.render(proxyparams, paramconf),
    "utf8"
  );
  if (!fs.existsSync(paramconf.nginx.logs)) fs.mkdirSync(paramconf.nginx.logs);
  paramconf.nginx.firstinstall = true;
  fs.outputJsonSync("../tribes/conf.json", paramconf, {
    space: 2,
  });

  return Www.create(paramconf.nginx);
};
*/
Wwws.create = (paramnginx) => {
  /**
   * Create an nginx conf to make available a spaceweb for a tribe /www/appname/
   *
   */
  const res = {
    status: 200,
    ref: "Www",
    msg: "successfulwww",
    data: { website: paramnginx.website },
  };
  const nginxwebsite = fs.readFileSync(
    `${conf.dirapi}/adminapi/www/adminapx/conf/nginxmodelwebsite.conf.mustache`,
    "utf8"
  );
  fs.outputFileSync(
    `./${paramnginx.fswww}www/nginx_${paramnginx.website}.conf`,
    mustache.render(nginxwebsite, paramnginx),
    "utf8"
  );
  if (!fs.existsSync(`./${paramnginx.fswww}www/${paramnginx.website}`)) {
    //See later how to generate specific template of webapp
    fs.mkdirSync(`./${paramnginx.fswww}www/${paramnginx.website}`);
  }
  if (!fs.existsSync(`./${paramnginx.fswww}www/cdn`)) {
    //See later how to generate specific template of webapp
    fs.mkdirSync(`./${paramnginx.fswww}www/cdn`);
  }
  //restart nginx
  const { exec } = require("child_process");
  exec(paramnginx.restart, (error, stdout, stderr) => {
    if (error) {
      if (paramnginx.firstinstall) {
        console.log("\x1b[42m", error, stdout, stderr, "x1b[0m");
      }
      //@todo supprimer la derniere config et relancer
      res.status = 500;
      res.msg = "nginxError";
      res.data = { msg: `${error}<br>${stdout}<br>${stderr}` };
    } else {
      if (paramnginx.firstinstall) {
        // the tribes/conf.json is saved in apxtriinstall
        console.log(
          `\x1b[42m###########################################################################################\x1b[0m\n\x1b[42mWellcome into apxtri, you can now 'yarn dev' for dev or 'yarn startpm2' for prod or       \n'yarn unittest' for testing purpose. Access to your town here \x1b[0m\x1b[32mhttp://adminapx\x1b[0m \x1b[42m             \nto finish your town setup. Don't forget to set your localhost /etc/hosts by adding 127.0.0.1 adminapx or {LAN IP} adminapx .  Check README's project to learn more.                          \x1b[0m\n\x1b[42m###########################################################################################\x1b[0m`
        );
      } else {
        // add website to tribe conf
      }
    }
  });
  return res;
};
Wwws.setssl = () => {
  // Run process to change nginx conf to get a ssl
};

Wwws.configlist = (tribeId) => {
  //if accessright R return list of conf parameter {webapp:{conf parameter}}
  const res = { status: 200, data: {} };
  return res;
};

module.exports = Wwws;

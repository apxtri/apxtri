## apxtri  Decentralized Autonomous Organisation (DAO)

Welcome to the technical side of apxtri, where you'll learn how it works and how you can contribute. Visit [apxtri web site](https://apxtri.crabdance.com) to discover how to create a new social world with apxtri.


## apxtri Architecture: a quick view & keywords definition

*   **a nation** is a topology network of nodes town that share the same fundamental rules (engrave into a common blockchain).
*   **a town** is a physical installation node of linux server plug into a LAN network (a telecom box NAT conf) accessible from a WAN network (domain name redirect to a town public IP).
*   **a tribe** has a unique name and is a space to host data to apply specific algo rule register at the creation of a tribe by a druid that respect the town rules
*   **a pagan** 'village farmer' is a unique identification create by anyone based on PGP (public/Private key) and a unique alias (for human) that allow a member to proof that he owns this identity. Can be anonymous but all his actions are store into a blockchain of reputation link to this unique ID.
*   **a druid** 'rules master' is a pagan that owned and defined rules of a tribe, he owns a domain name (DNS) to make it tribe's data accessible over the net. As pagan he can be anonymous, he is responsible of the tribe activities and can ban a pagan of his tribe. Any pagan become a druid when he decides to create a tribe by requesting a town mayor
*   **a mayor** is a owner of a physical machine, he is in charge to make a town node working (IP availibily) and accept to be part of a nation (rules of the blockchain nation).
*   **a contract** is an algorythme that is written into the blockchain and will be run if an action trig. Immutable contract are the one link to the blockchain. Any pagan/druid/major can defined a contract that apply to other pagans.
*   **the XBE coin** is the token that drive the blockchain, it materialises an exchange value and is a conterpoint
*   **a git apxtri** is a package ready to install a town by a mayor into a physical server that will be able to join a nation (mean accept thoses nation rules). You can also create a nation and stay alone or not. Then mayor will be able to host druid request to create tribe. Then druid will invite pagan to join his tribe...

All actors will have the same target to respect contracts and are free to leave or to stay into a nation, a town or a tribe. Unfair contracts lead to empty towns, tribes, or nations, resulting in no value creation. Only fair rules will survive, rewarding actors with XTRIB coins.

```plaintext
/town-nation/                          tribe sapce
  /conf.json                           town conf express.js   
  /tmp/tokens                          temporary files                
  /adminapi/                           Space to manage tribes
           /apxtri/                   core system that manage used tribe adminapi referential endepoint https://.../api/routeName/...
                /middlewares
                /models                  
                /routes                endpoint that return {status,ref,msg,data}
                apxtri.js              starting point
                package.json  
            /nginx/adminapi_adminapx.conf   nginx conf per website 
            /schema/                    list of schema for ObjectName.json title description coments are in english
                  /lg                    list of schema ObjectName_lg.json per language data (same structure than ObjectName.json but in language lg)  
            /objects/objectName/
                    /idx/    list of indexName.json 
                    /itm/    list of object content store by apxid.json (unique key to identify an ite in a collection of items object 
                    /conf.json   Version list and schema link that define this object  

            some key objects       
                    /pagans/         Unique numeric ID shared accross all node (towns)
                    /towns/          Unique town name shared accross all node by domain name + IP
                    /...
                    /wwws/           Webspace that can be served with nginx
                          cdn/       web public access file
                         /adminapx/index_lg.html administration webapp
                         /website/
  /idx/                   
  /itm/adminapi.json                      tribe conf admin 
       tribeName.json                     tribeName conf
         /tribeName/
            /apxtri/routes                 Specific web service https://.../api/tribename/routeName/endpoint
                /middlewares
                /models 
            /nginx/tribe_website.conf      nginx conf per website    
            /schema/
            /objects/
            /www/
            ...
```

## Network topology

 As it is decentralize organisation, a public address directory  is replicated on each instance. We strongly recommand to use many reverse proxy to manage your public IP, never exposed your physical real IP in your country for anonymous target.   
 Each town (instance) is accessible with an anonymlous DNS https://apxtri.crabdance.com where the IP adresse is update when elected town's IP to register a new block into the blockchain (each 10minutes).

## Quick start:

Pre-request: linux maxhine on a vps or physical machine ( we choose ubuntu server > 20, adapt this if you want to use something else).

To start you need to 

*   setup your network and for production a Domain Name Server (DNS) and a SSL certificat to your town's IP
*   setup your machine by installing snap, nginx, nvm, npm, node

As a mayor, you have to understand what you are doing, your numeric reputation is in the game. Always check sources before downloading any things. For dev use town name: **dev** and nation name :**ants** 

**Machine:**

**use a sudoer user and execute command line $, for security reason, do not use apxuser replace it by something not common ( not as admin, mayor,… use your nickname or pseudo)**

```plaintext
$ sudo apt update
$ sudo apt upgrade
$ sudo useradd -s /bin/bash -m -d /home/{apxuser} -c "{apxuser}" {apxuser}
$ sudo passwd {apxuser}
$ sudo usermod -aG sudo {apxuser}
$ sudo visudo
# Add the following line to the file:
$ {apxuser} ALL=(ALL) NOPASSWD: ALL
# Exit and save, then switch to the new user:
$ su {apxuser}
```

For Production

### Network: 

FOR DEV:  you can just use local http:/dev-ants

```plaintext
$ sudo vim /etc/hosts # add 127.0.0.1 dev-ants
# Open http://dev-ants in your browser after installation.
```

FOR PRODUCTION: 

You need a domain name. To get a free one, visit [http://ydns.io] and create one like apxtri.ydns.io that contains your IP address. Then, update your URL with something like [https://ydns.io/hosts/update/Tl7FDQAETmQre312edztgsI](https://ydns.io/hosts/update/Tl7FDQAETmQre312edztgsI)[Uy](https://ydns.io/hosts/update/Tl7FDQAETmQre312edztgsIUy)

```plaintext
# Into your production machine
$ crontab -e
# add a line at the end to update your IP in case you have a dynamic WAN IP
1,6,11,16,21,26,31,36,41,46,51,56 * * * * sleep 38 ; wget --no-check-certificate -O - https://ydns.io/hosts/update/Tl7FDQAETmQre312edztgsIUy &gt;&gt; /tmp/apxtri_ydns.io.log 2&gt;&amp;1
$ ip -4 address  # to get your (local) LAN IP of your server
```

Connect in your browser to your box (use your LAN IP and change last number by 1) Set as a dmz your local LAN IP and route (NAT) your internet trafic port 80 and 443 to this local IP.

This allow public access from internet to your DMZ local server.  Think to use reverse proxy to be more anonymous. Contacts the community to help keeping you anonymous if you want.

### Machine: 

```plaintext
$ sudo apt install git vim libcap2-bin p7zip-full p7zip-rar curl nginx
# Install the latest version of nvm (check the website for the latest version: https://github.com/nvm-sh/nvm)
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
# Close and re-open the terminal under the apxuser
$ nvm --version
# If nvm is not found, copy and paste the last 3 lines of the installation script: export NVM_DIR=....
# Then recheck the nvm version
$ nvm --version
# Install Node.js, npm, and yarn. Follow the online instructions to update the configuration properly.
$ nvm install node
$ node --version # to check the Node.js version
$ npm install --global yarn
$ yarn --version # to check the yarn version
# Find a non-existing town to join an existing nation. See https://apxtri.crabedance.com
$ mkdir ~/apxtowns # if it does not exist
$ mkdir ~/apxtowns/{town}-{nation}
$ sudo chown {apxuser}:root /etc/nginx/nginx.conf
#################################
# For Dev ###############
#################################
$ mkdir ~/apxtowns/{town}-{nation}/adminapi/
$ cd ~/apxtowns/{town}-{nation}/adminapi/
$ git clone https://gitea.ndda.fr/apxtri/apxtri.git
$ yarn install

# Set up and run for the first time
$ dns=apxtri.ydns.io user=apxuser yarn dev



#################################
# For production  ###############
#################################
# rename your machine not mandatory but usefull
$ sudo vim /etc/hostname  #replace name by town-nations
$ sudo vim /etc/hosts     # replace previous hostname by town-nations
$ sudo reboot
# ssl cerificat management for https
$ sudo snap install core
$ sudo snap refresh core
$ sudo apt remove certbot
$ sudo snap install --classic certbot
$ sudo ln -s /snap/bin/certbot /usr/bin/certbot
$ curl -L https://wall-ants.ndda.fr/cdn/share/apxtriVx.tar | tar -xf - -C ~/apxtowns/town-nation/ | cd ~/apxtowns/town-nation/tribes/adminapi/apxtri | yarn install
$ town=townname nation=nationname dns=apxtri.ydns.io yarn startapx # for 1st install to set /etc/nginx/nginx.conf conf/nginx/adminapi_adminapx.conf with your parameter
# FYI: it changes nginx configuration + let's encrypt your dns to get a ssl
$ yarn restartapx  # if IT IS NOT a first install
# open a browser and type https://domain to access to your admin console

# To set autostart in case of reboot
$ yarn pm2 list    # To check if all is fine
$ yarn pm2 startup # If needed it will request from you to run a command
$ sudo env PATH=$PATH:/home/apxuser/.nvm/versions/node/v17.3.0/bin /home/apxuser/apxtowns/town-nation/tribes/adminapi/apxtri/node_modules/pm2/bin/pm2 startup systemd -u apxuser --hp /home/apxuser

# To stop autostart
$ yarn pm2 unstartup ; in case of error follow the command line provide like)
$ sudo env PATH=$PATH:/home/apxuser/.nvm/versions/node/v17.3.0/bin /home/apxuser/apxtowns/town_nation/tribes/adminapi/apxtri/node_modules/pm2/bin/pm2 unstartup systemd -u apxuser --hp /home/apxuser

# As mayor you need to own this town as yours by conecting to https://dns
```

## apxtri Documentations

### Documentation for developper: 

apxtri core : https://dns/cdn/apidoclist all endpoint and how authentification and data object management works.

*   anonymous numeric identity with **pagan** object that allow a unique alias and publickey
*   how a pagan can become a **person** for a trib (anonymously or not)
*   how notification can cypher message between 2 pagans to communicate
*   how a dev can create and host a tribe and a full project with api backend (documented with apidoc) and a webapp (in react, vue, angular, or a simple web page)

### Main end-points:

*   Odmdb : data model management and accessright
*   Pagans : numeric Identity with person to join a tribe
*   Notifications: Exchange secure  message between Pagans and machine (alert,…)
*   Tribes; crud a tribe for a druid
*   Trackings: nginx watching to manage security and ban unwanted user from ennemy's IP (non libertarian user)
*   Middleware: to manage authentification and data accessright process.
*   Blockchain: to synchronize common data and manage contracts between Pagans (as well than managing  an api crypto wallet)
*   Multilanguage management

### Documentation for mayor and druid:

A webapp allow you to manage a town as mayor or as a druid access it and authentify you on https://dns/ 

## ROAD MAP

If you want to join our task force to make this libertarian project live feel free to contact us on discord in apXtri server 

We are looking for dev (node.js, express.js, js, web developper), for translator, for trainner, for mayor and druid from any countries  that want developp a business with their machine/bandwidth and local communities.

Dev

*   Dev a front end to manage apXtri for druid and mayor (CRUD a town a tribe)
*   Dev a synchronization process between node (towns) to load balance and make uncensurable druid web space
*   Dev contracts / crypto with a wallet for a mayor to sale crypto againt fiat currecny and allow a druid to rent anonymously web space.
*   Dev a marketplace front to share objects (product or service) between any pagans / persons by inverting the process of  offer / demand

Community managers and user

*   animate the apXtri discord and social network to dev the user community
*   translate content app for italian, german, spanish,
*   creator or producer of any valueable stuff to sale/exchange any things into your community

Trainer / Teacher

*   Teach blockchain and alternative currency system (Monero, bitcoin, xbe…) and
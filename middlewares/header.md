## api users and backend developers

api documentation for routes and middleware has to respect apidoc's rules [https://apidocjs.com/](https://apidocjs.com) 

To update this doc accessible in [https://wal-ants.ndda.fr/apidoc](https://wal-ants.ndda.fr/cdn/apidoc) :

 `$ tribe=adminapi yarn apidoc` 

For api tribe's doc  accessible in [https://admin.smatchit.io/apidoc](https://smatchit.io/cdn/apidoc) [:](https://smatchit.io/cdn/apidoc:) 

`$ tribe=smatchit yarn apidoc`

To get overview check README.md project and the package.json [https://gitea.ndda.fr/apxtri/apxtri](https://gitea.ndda.fr/apxtri/apxtri)

A special tribe call adminapi in any towns (node), it works the same than all the other tribe except that all their data are synchronize with a blockchain 

Objects manage by adminapi are: pagans (numerique id =alias/public key / private key), notifications (cypher message betxeen alias) , nations (rules apply to all towns belonging to a nations), towns ( a server that host IT ressources disk space, ram, bandwith and rules aplly to all tribe belonging to a town), tribes (( a sharing space to store data as well as api with rules to any person that use it), wwws (web space, dns)

## Object management (Odmdb)

An object has a name and is defined by a schema that contain properties key.

A propertie has a name and a list of caracteristics (type, pattern,format,...) that have to be validate to be accepted.  
All properties respect the rules [https://json-schema.org/draft/2020-12/schema,](https://json-schema.org/draft/2020-12/schema,) some extra"format" can be add to mutualise recurrent regex pattern

To access a schema [https://wall-ants.ndda.fr/api/adminapi/schema/tribename/schamname.json](https://wall-ants.ndda.fr/nationchains/schema/nations.json) and language is set by the header in xlang

A checkjson.js is available to manage all specific format [https://wall-ants.ndda.fr/Checkjson.js](https://wall-ants.ndda.fr/Checkjson.js) see **Odmdb - schema Checkjson**

**required**: an array of required properties

**Additional properties that not exist in 2020-12/schema :**

**apxid**: the propertie used as an unique id

**apxuniquekey**: array of unique properties

**apxidx** : array of index definition

**apxaccessrights**: object with key profilname and accessrights on properties {profilname:{C:\[properties array\],R:\[properties array\],U:\[\],D:\[\]}}

Items of an object are store in files into :  

```plaintext
tribename/objectnames/idx/keyval_objkey.json
tribename//objectnames/itm/uniqueid.json
```

## Accessrights:

An alias is just an identity, to access a tribe, a person must exist with an authenticated alias into /tribes/{tribename}/objects/persons/itm/{alias}.json

A person has a property profils with a list of profilename, common profiles are : anonymous (no identity) / pagan (an identity)  / person (an identity with access right into a tribe) / druid (the administrator of a tribe) / mayor (administrator of a town/server)/ and any profil can be define for a tribe

Each object has an apxaccessrights that is a list of profil and CRUD access per object key.

Example: owner on this object cab create delete an item is own, can read a list of propertie and update only some.

```plaintext
"owner": {
      "C" : [],
      "D": [],
      "R": ["alias","owner","profils","firstname","lastname","dt_birth"],
      "U": ["firstname","lastname","dt_birth"]
    }
```

## api pre-request

API Endpoint url: **/api/{tribename}/{routename}/xxx**

Domaine name can be a adminapi donaim name aswell any tribe's domain  name. Check nginx conf in /tribename/nginx 

**Valid header see Middlewares**

App use openpgp.js lib to sign xalias\_xdays  (xdays a timestamp integer in miilisecond from Unix Epoch) with a privatekey and store it in xhash.

/middlewares/isAuthenticated.js check if (xhash) is a valid signature of the public key a xhash is valid for 24 hours

See Pagans models that contain authentification process

**api Return in 3 data structure:**

A - data file from a classical get  [https://wall-ants.ndda.fr/Checkjson.js](https://smatchit.io/Checkjson.js)

B -  a json single answer **{status, ref,msg,data}:**

*   status: http code return
*   ref: model/route name reference where message come from
*   msg: a message template key store into models/lg/name\_lg.json (where lg is 2 letters language)
*   data: an object data use to render the value of the message key.

C - a json multi answer **{status,multimsg:\[{ref,msg,data}\]}**

         Each {ref,msg,data\] work the same way than B

To show feedback context message in a language lg => get /api/adminapi/objects/tplstrings/{{model}}\_{{lg}}.json  
This contain a json {msg:"mustache template string to render with data"}  

## Add tribe's api:

Accessible with https://dns/api/tribename/routename/

```plaintext
/tribes/tribename/apxtri/routes
/tribes/tribename/apxtri/middlewares
/tribes/tribename/apxtri/models
/tribes/tribename/schema
/tribes/tribename/schema/lg
```

```plaintext
// Example of a route
const tribe="smatchit";
const conftrib = require(`../../../adminapi/objects/tribes/itm/${tribe}.json`);
const conf = require(`../../../adminapi/objects/tribes/itm/adminapi.json`);
const express = require(`../../../adminapi/apxtri/node_modules/express`);
const fs = require(`../../../adminapi/apxtri/node_modules/fs-extra`);
const Nofications = require(`../../../adminapi/apxtri/models/Notifications.js`);
const Appscreens = require(`../models/Appscreens`);
const router=express.Router();
module.exports=router;
```